package com.company;

import java.util.ArrayList;

public class FileServer {
    ArrayList<AMMFile> files = new ArrayList<AMMFile>();
    public void upload(User owner, String fileName, int fileType, byte[] data) {
        AMMFile mmFile = FileFactory.createFile(owner,fileName,fileType, data);
        files.add(mmFile);
    }

    public AMMFile getFile(int index){
        /* get the file from the list and return it*/
        return files.get(index);
    }
}
