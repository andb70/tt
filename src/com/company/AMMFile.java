package com.company;

import java.util.Date;

public abstract class AMMFile implements IFile{
    String fileName;
    private User owner;
    private Date creationDate;
    private int fileType;
    byte[] data;

    AMMFile(User owner, String fileName, byte[] data){
        this.owner = owner;
        /*...*/
     }

    public User getOwner() {
        return this.owner;
    }

    public String getFileName() {
        return this.fileName;
    }

    public Date getCreationDate() {
        return this.creationDate;
    }

    public byte[] getData() {
        return this.data;
    }

    public void setData(byte[] newData) {
       /* some op */
    }

    public void save() {
        /* some op */
    }
}
