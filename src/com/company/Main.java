package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Integer a[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        Main.shuffle(a);
        for (int i=0; i<a.length; i++) {
        System.out.println(a[i]);}

        System.out.println("2016.1.31: " + Main.valiDate(2016,1,31));
        System.out.println("2016.2.28: " + Main.valiDate(2016,2,28));
        System.out.println("2016.2.29: " + Main.valiDate(2016,2,29));
        System.out.println("2016.2.30: " + Main.valiDate(2016,2,30));
        System.out.println("2016.1.32: " + Main.valiDate(2016,1,32));
        System.out.println("2016.13.29: " + Main.valiDate(2016,13,29));
        System.out.println("1600.2.29: " + Main.valiDate(1600,2,29));
        System.out.println("1700.2.29: " + Main.valiDate(1700,2,29));
        System.out.println("2000.1.29: " + Main.valiDate(2000,1,29));
        System.out.println("2000.2.29: " + Main.valiDate(2000,2,29));
        System.out.println("2000.4.31: " + Main.valiDate(2000,4,31));
        System.out.println("2000.5.31: " + Main.valiDate(2000,5,31));
        System.out.println("1700.2.12: " + Main.valiDate(1700,2,12));
        System.out.println("1700.2.28: " + Main.valiDate(1700,2,28));

        System.out.println("08106710158: " + Main.validPIva("08106710158"));
        System.out.println("06645501005: " + Main.validPIva("06645501005"));


    }
    public static <E> void  shuffle (E a[]) {
        for (int i=0; i<a.length; i++) {
            E tmp;
            int j = (int)(Math.random()*a.length);
            tmp = a[i];
            a[i] = a[j];
            a[j] = tmp;
        }

    }

    public static boolean validPIva(String pIva) {

        try {
            int x=0, y=0, z=0;
            for (int i=0; i<10; i+=2){
                x+=(pIva.charAt(i)-48);
                y+=(pIva.charAt(i+1)-48)*2;
                if (pIva.charAt(i+1)>52) z++;
            }
            int t = (x+y+z) % 10;
            int r = pIva.charAt(10)-48;

            return r == (10 - t) % 10;
        } catch (Exception e) {
            return false;
        }
    }


    public static boolean valiDate(int year, int month, int day) {
        boolean vYear = year > 1581 && year < 3000;
        boolean vMonth = month > 0 && month < 13;

        boolean vDay31 = day > 0 && day < 32;
        boolean vDay30 = month == 1
                        || month == 3
                        || month == 5
                        || month == 7
                        || month == 8
                        || month == 10
                        || month == 12
                        || day < 31;
        boolean vDayFeb = month!=2
                        || day < 29
                        || (year % 4 == 0 && year % 400 > 0 && day < 30);

        return vYear && vMonth && vDay31 && vDay30 && vDayFeb;
    }
}
