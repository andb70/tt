package com.company;

import java.util.Date;

public interface IFile {
    User getOwner();
    String getFileName();
    int getFileType();
    Date getCreationDate();
    byte[] getData();
    void setData(byte newData[]);
    void save();
}
