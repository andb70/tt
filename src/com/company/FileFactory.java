package com.company;

public class FileFactory {
    public static AMMFile createFile(User owner, String fileName, int fileType, byte[] data) {
        switch (fileType) {
            case 0:
                return new MMDoc(owner, fileName, data);
            case 1:
                return new MMVideo(owner, fileName, data);
        }
        return null;
    }
}

