package com.company;

public class MMVideo extends AMMFile{
    private long duration;

    MMVideo(User owner, String fileName, byte[] data) {
        super(owner, fileName, data);
    }

    public int getFileType() {
        return 1;
    }
    public long getDuration() {
        return duration;
    }

}
